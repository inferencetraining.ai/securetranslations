import torch
from sentence_transformers import SentenceTransformer, util
from torch import optim
from transformers import MBartTokenizerFast, MBartForConditionalGeneration





class OptimzeTranslation:
    def __init__(self):
        self.target = 'translation'
        self.model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
        self.main_model = MBartForConditionalGeneration.from_pretrained(model_name)

    def sentence_distance(self, sentencex, sentencex1):
        embedding_1 = self.model.encode(sentencex, convert_to_tensor=True)
        embedding_2 = self.model.encode(sentencex1, convert_to_tensor=True)
        dist = util.pytorch_cos_sim(embedding_1, embedding_2)
        return(dist)
        

    def loss_fn(self, sample):
        samp_loss = self.sentence_distance(sample, self.target)
        return(torch.tensor([samp_loss*-1]))
    

    def Optimize(self, sample, target, max_epoch):
        sample = self.main_model(sample)
        performance = 0
        for epochs in range (max_epoch):
            optimizer = optim.Adagrad(self.model.parameters())
            optimizer.step(closure=self.loss_fn)
            state_dict = optimizer.state_dict() 
            self.main_model.parameters = state_dict
            performance = epochs
        
        return(performance)
